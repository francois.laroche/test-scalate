package com.ingenico.scalate.test

import com.samskivert.mustache.Mustache
import com.samskivert.mustache.Mustache.{Collector, Formatter, VariableFetcher}

import java.util
import scala.jdk.CollectionConverters._

object JMustacheTest extends App {

  val data = Map("value" -> template3Data).asJava
  val mustache = Mustache.compiler
  val defaultFormatter = mustache.formatter
  val formatter = new Formatter() {
    override def format(value: Any): String = {
      value match {
        case None    => ""
        case Some(v) => defaultFormatter.format(v)
        case other   => defaultFormatter.format(other)
      }
    }
  }
  val defaultCollector = mustache.collector
  val collector = new Collector {
    override def toIterator(value: Any): util.Iterator[_] =
      defaultCollector.toIterator(value)

    override def createFetcher(
        ctx: Any,
        name: String
    ): Mustache.VariableFetcher = {
      ctx match {
        case Some(v) => createFetcher(v, name)
        case None    => (_: Any, _: String) => None
        case p: Product =>
          (_: Any, name: String) => {
            p.productElementNames.toSeq.zipWithIndex.find { case (n, _) =>
              n == name
            } match {
              case None => None
              case Some((_, index)) =>
                p.productElement(index).asInstanceOf[AnyRef]
            }
          }

        case other => defaultCollector.createFetcher(other, name)
      }
    }

    override def createFetcherCache[K, V](): util.Map[K, V] =
      defaultCollector.createFetcherCache()
  }

  val startTime = System.currentTimeMillis()
  val template = Mustache.compiler
    .withFormatter(formatter)
    .withCollector(collector)
    .compile(mustacheTemplate3)

  val result: String = template.execute(data)
  val endTime = System.currentTimeMillis()

  println(result)
  println(s"Processing took ${endTime - startTime}ms")
}
