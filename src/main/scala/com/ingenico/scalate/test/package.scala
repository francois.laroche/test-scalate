package com.ingenico.scalate

package object test {
  final case class Test(name: String, value: Int)
  final case class Test2(name: String, value: Option[Int], value2: Option[Int])

  final case class NestedWithOptions(test1: Option[Test], test2: Option[Test])

  val mustacheTemplate: String =
    """
      |Hello {{value.name}}
      |You have just won ${{value.value}}!
      |""".stripMargin

  val mustacheTemplate2: String =
    """
      |Hello {{value.name}}
      |You have just won ${{value.value}}!
      |You have just won ${{value.value2}}!
      |""".stripMargin

  val mustacheTemplate3: String =
    """
      |Hello {{value.test1.name}}
      |You have just won ${{value.test2.value}}!
      |""".stripMargin

  val template3Data: NestedWithOptions =
    NestedWithOptions(Some(Test("aa", 42)), None)
}
