package com.ingenico.scalate.test

import com.github.mustachejava.util.Wrapper
import com.github.mustachejava.{
  Binding,
  Code,
  DefaultMustacheFactory,
  Iteration,
  Mustache,
  ObjectHandler,
  TemplateContext
}

import java.io.{StringReader, StringWriter, Writer}
import java.util
import java.util.stream.Collectors
import scala.jdk.CollectionConverters._

object MustacheTest extends App {
  val mf = new DefaultMustacheFactory()
  val handler = mf.getObjectHandler
  mf.setObjectHandler(new ObjectHandler {
    override def find(name: String, scopes: util.List[AnyRef]): Wrapper = {
      handler.find(name, scopes)
    }

    override def coerce(value: Any): AnyRef = {
      handler.coerce(value)
    }

    override def iterate(
        iteration: Iteration,
        writer: Writer,
        current: Any,
        scopes: util.List[AnyRef]
    ): Writer = {
      handler.iterate(iteration, writer, current, scopes)
    }

    override def falsey(
        iteration: Iteration,
        writer: Writer,
        current: Any,
        scopes: util.List[AnyRef]
    ): Writer = {
      handler.falsey(iteration, writer, current, scopes)
    }

    override def createBinding(
        name: String,
        tc: TemplateContext,
        code: Code
    ): Binding = {
      val delegate = handler.createBinding(name, tc, code)
      scopes => {
        delegate.get(
          scopes
            .stream()
            .map {
              case Some(v: AnyRef) => v
              case other           => other
            }
            .collect(Collectors.toList[AnyRef])
        )
      }
    }

    override def stringify(value: Any): String = {
      value match {
        case None    => ""
        case Some(v) => stringify(v)
        case other   => handler.stringify(other)
      }
    }
  })

  val data = Map("value" -> template3Data).asJava

  val startTime = System.currentTimeMillis()
  val mustache: Mustache = mf.compile(
    new StringReader(mustacheTemplate3),
    "template.mustache"
  )
  val writer = new StringWriter()
  mustache.execute(writer, data).flush()
  val result = writer.toString
  val endTime = System.currentTimeMillis()

  println(result)
  println(s"Processing took ${endTime - startTime}ms")

}
