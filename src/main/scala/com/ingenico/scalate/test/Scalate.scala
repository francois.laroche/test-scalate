package com.ingenico.scalate.test

import org.fusesource.scalate.{RenderContext, TemplateEngine, TemplateSource}

object Scalate extends App {
  private val values = Map("value" -> Test2("aa", Some(42), None))

  val engine = TemplateEngine(None, "production")
  engine.boot()

  val startTime = System.currentTimeMillis()
  val source = TemplateSource.fromText("template.mustache", mustacheTemplate2)
  val result = engine.layout(source, values)
  val endTime = System.currentTimeMillis()

  println(result)
  println(s"Code took ${endTime - startTime}ms")

}
