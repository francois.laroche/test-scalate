package com.ingenico.scalate.test

import com.github.jknack.handlebars.context.{
  MapValueResolver,
  MethodValueResolver
}
import com.github.jknack.handlebars.{
  Context,
  Formatter,
  Handlebars,
  Template,
  ValueResolver
}

import java.util
import java.util.{Collections, Map => JavaMap}
import scala.jdk.CollectionConverters._

object HandlebarsTest extends App {
  val handlebars: Handlebars = new Handlebars().`with`(new Formatter {
    override def format(value: Any, next: Formatter.Chain): AnyRef = {
      value match {
        case None        => ""
        case Some(value) => next.format(value)
        case other       => next.format(other)
      }
    }
  })

  val scalaResolver = new ValueResolver {
    override def resolve(context: Any, name: String): AnyRef = {
      context match {
        case None => None
        case Some(p: Product) =>
          p.productElementNames.toSeq.zipWithIndex.find { case (n, _) =>
            n == name
          } match {
            case None => None
            case Some((_, index)) =>
              p.productElement(index).asInstanceOf[AnyRef]
          }
        case _ => ValueResolver.UNRESOLVED
      }
    }

    override def resolve(context: Any): AnyRef = {
      context match {
        case None             => None
        case Some(v: Product) => v.asInstanceOf[AnyRef]
        case _                => ValueResolver.UNRESOLVED
      }
    }

    override def propertySet(
        context: Any
    ): util.Set[JavaMap.Entry[String, AnyRef]] = {
      Collections.emptySet()
    }
  }

//  val data = Map("value" -> Test2("aa", Some(42), None))
  val data = Map("value" -> template3Data)

  val startTime = System.currentTimeMillis()
  val context = Context
    .newBuilder(data.asJava)
    .resolver(
      scalaResolver,
      MapValueResolver.INSTANCE,
      MethodValueResolver.INSTANCE
    )
    .build();
  val template: Template = handlebars.compileInline(mustacheTemplate3)
  val result: String = template(context)
  val endTime = System.currentTimeMillis()

  println(result)
  println(s"Processing took ${endTime - startTime}ms")
}
