# Templating Test

This project is meant to compare templating engines

Engines like:
- twirl
- freemarker

were not chosen since their approach is to have all templates at a place and compile them.

The technologies are:
- scalate
- Handlebars
- Mustache.java
- jmustache

for all technologies, the following must be handled:
- using options, at the leaf level or in the middle of the path
- using case classes

The response time should stay small.