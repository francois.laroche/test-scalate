name := "scalate-test"
organization := "com.ingenico"
version := "0.0.1-SNAPSHOT"

scalaVersion := "2.13.8"

libraryDependencies ++= Seq(
  "org.scalatra.scalate" %% "scalate-core" % "1.9.8",
  "com.github.jknack" % "handlebars" % "4.3.0",
  "com.github.spullara.mustache.java" % "compiler" % "0.9.10",
  "com.samskivert" % "jmustache" % "1.15"
)
